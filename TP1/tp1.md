# Cours Forge ZZ2 F2 2017

# Infos étudiant :

Par binôme Nom Prénom:
 * Nom Prénom
 * Nom Prénom


## Première partie - Consignes générales
### Format du document

Ce TP est et son compte rendu est un fichier au format `md` pour **Markdown** qui est un format permettant de gérérer facilement du HTML. Ce format étant couramment supporté par les outils de développement, nous l'utiliserons pour les comptes rendus.

Plus d'information sur la syntaxe de ce format :
* Spécification : https://daringfireball.net/projects/markdown/
* Cheatsheet : https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
* Version GitLab : https://docs.gitlab.com/ee/user/markdown.html#code-and-syntax-highlighting

Ci dessous un exemple d’éditeur à utiliser pour obtenir une prés visualisation de votre document :
* **Atom** avec le plugin **markdown-preview**
  * ctrl-shift-M pour afficher la preview
* **Notepad++** avec le plugin **MarkdownViewerPlusPlus**
* **SublimeText** avec le plugin **sublimetext-markdown-preview**
* **Eclipse**
* **IntelliJ**
* **vi / blocnote**
* ...

* Snipplet de code

### Format md

#### Exemple de code
```java
  // ceci est un commentaire
  public String test = "Hello Word";
  boolean hello = true;
  if (true == hello ) {
    System.out.println(test);
  }
```
##### Utilisation de liste
0. item 1
  0. item 1.1
0. item 2


* liste
* non ordonné
  * sans index

##### Accentuation

Italic avec *étoile* or _tiret-bas_.

En gras avec double **étoile** ou __tiret-bas__ .

Il est possible de combiner les deux **étoile et _tiret-bas_**.

Pour barrer on utilise deux tildes. ~~Barrer ça.~~

##### Liens
[Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

##### Tables
| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

Markdown | Less | Pretty
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3


### Exemple pour le compte rendu

Compléter le document avec vos réponses, pour chaque consignes indiquez les commandes utilisées,le résultat et un commentaire. Par exemple :
> #### 1. Création d'un répertoire, ajout d'un fichier et lister le répertoire
> ##### Listing des commandes et résultat
    user@localhost:~/$ mkdir temp
    user@localhost:~/$ cd temp/
    user@localhost:~/temp$ ls
    user@localhost:~/temp$ touch newFile
    user@localhost:~/temp$ ls
    newFile
> ##### Commentaire
> Création d'un nouveau dossier **temp** dans la répertoire courant et ajout ajout d'un fichier **newFile**, nous avons utiliser la commande `ls` pour lister le contenu de ce nouveau répertoire.


## TP 1


Télécharger le sujet du TP à l'adresse suivante :
https://gitlab.isima.fr/mazenovi/2017-F2-Forge

#### 1. Les basiques
0. Créer un repository git
0. _Par la suite pensez à commiter votre compte rendu apres chaques étapes dont l'enoncé est terminé par `*`, pour les messages de commit vous pouvez utiliser les numéros des questions (Ex Q1.2 ici)_
0. Ajouter le compte rendu dans le repo `*`
0. Afficher la status de votre copie de travail
0. Modifiez le CR pour supprimer la première partie et indiquez vos noms (n'oublier pas de faire un `git add` pour marque le fichier comme devant être ajouté) `*`
0. Afficher l'historique de votre repo
0. Renommer ce fichier en préfixant avec vos noms en amendant le commit précédent (`git commit -m "mon message"` pour indiquer le mesage de commit directement) `*`
0. Afficher l'historique de votre repo
0. Il ne devrait y avoir que deux entrées dans l'historique pourquoi ? `*`
0. Créer un nouveau fichier, nommé start, contenant la date et l'heure actuelle `*`
0. Créer un nouveau fichier : file2ignore
0. Afficher la status de votre copie de travail
0. On souhaite que ce fichier soit ignoré et ne soit jamais commiter. Réalisez la configuration nécesaire pour que cette regle soit effective `*`
0. Lister le contenu du repertoire courant, afficher le status et la log
0. Avant de commiter affichez les modifications par rapport à la précédente révision ? `*`

##### 2. Les branches
0. Créez une branche portant votre nom et basculer sur cette branche (dans la suite du TP cette branche est désignée par `mybranch`)
0. Lister les branches locales et les fichiers présent dans le répertoire courant `*`
0. Aficher le status de votre repo `*`
0. Créez une branche pour _développer_ la réponse de ce point (nommez là `mybranch-2.4` par exemple)
  0. Afficher un historique sous forme de graph (`a dog`) de votre repo
  0. Pourquoi les 2 branches pointent elles sur la même révision ?`*`
  0. Afficher à nouvea l'historique pour montrer les modifications suite au précédent commit `*`
0. Revenir la brache `mybranch`
0. Où sont passé vos reponces au point 2.4 ? `*`
0. Affichez un historique sous forme de graph (`a dog`) de votre repo, Que peux ton en dire ?
0. Revenir sur la branche master
0. Ajoutez et commitez un fichier (touch new_file)
0. Revenir sur votre branche `mybranch` pour completer le compte rendu `*`
0. Affichez un historique sous forme de graph (`a dog`) de votre repo `*`

##### 3. Merge
0. Merge depuis head
  0. switcher sur une nouvelle branche `mybranch-3.1`
  0. ajout un nouveau fichier nommé easy_merge avec la date et l'heure actuelle `*`
  0. merger la branche `mybranch-4.1` sur `mybranch`
0. Afficher le status
0. Pourquoi n'y a t'il aucune modification en cours ? `*`
0. Affichez un historique sous forme de graph (`a dog`) de votre repo et décire l'état courant`*`
0. Merge avec modifications
  0. Mergez les modifications de la branche `mybranch-2.4` sur `mybranch` (`*echap* :wq *enter*` pour sauvegarder le message de commit et quitter)`*`
  0. Affichez un historique sous forme de graph (`a dog`) de votre repo `*`
0. Merge avec conflit
  0. Céer une nouvelle branche `mybranch-3.6`
  0. Notez dans le CR la date et l'heure actuelle (avec la commande `date` par exemple) `*`
  0. Switchez sur la nouvelle branche et modifiez la réponse précendante dans le CR avec le `*`
  0. Réalisez le merge de la brache `mybranch-3.6` sur `mybranch`. Le prompt change, pourquoi ? Gerer le conflit et commiter.
0. Supprimer les branches de feature et afficher toutes les branches restantes, affichez un historique sous forme de graph (`a dog`) de votre repo `*`

##### 4. Remote
0. Afficher l'historique de votre repo
0. Ajouter le projet "2017-F2-Forge" comme repository distant `*`
0. Lister les branches distantes
0. Pousser votre branche de votre repo local sur le repos distant `*`
0. En consultant votre branche sous gitlab, vous devriez constaté que la date de modification du fichier start est plus acienne que celle de votre compte rendu, porquoi alors que le push de toute votre branche est faite en une seule fois ?
0. Supprimer le dossier .git
0. Faire un git status ?
##### 5. Envoyer son CR
0. via une [merge request](https://docs.gitlab.com/ce/gitlab-basics/add-merge-request.html) poussez votre CR une fois celui ci terminé 
